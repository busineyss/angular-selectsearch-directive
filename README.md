AngularJS Select And Search Directive
===============================

Dependencies:

* Angular
* Angular Bootstrap
* Bootstrap 3 (css)

### Install with Bower ###

```html
sudo bower install https://bitbucket.org/busineyss/angular-selectsearch-directive.git --save --allow-root
```

In your html/template add 

```html
<script src="/bower_components/angular-selectsearch-directive/dist/angular-selectsearch-directive.js"></script>
```

In your application, declare dependency injection like so:

```javascript
angular.module('myApp', ['angularSelectSearch']);
```

#### Examples ####

```html
<angular-select-search
    options="view.rubrics" <!-- items list -->
    label="{{'Parent rubric'| translate}}"
    model='view.item.rubric_parent' <!-- id of selected model -->
    placeholder="{{'Select a parent rubric'| translate}}"
    fields="['title']" <!-- display field (in list items) -->
    remove="true" <!-- display clear button -->
></angular-select-search>
```