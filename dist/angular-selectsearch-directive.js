angular.module('angularSelectSearch', []).directive('angularSelectSearch', [
    '$rootScope',
    '$window',
    '$filter',
    '$uibModal',
    '$timeout',
    function ($rootScope, $window, $filter, $uibModal, $timeout) {
        var template = '';
        template += '<div class="form-group {{!view.isValid ? "has-error" : ""}}">';
        template += '<label class="control-label" ng-if="label">';
        template += '<i ng-if="!view.isValid" class="material-icons md-14">warning</i>{{label}}<sup ng-if="req">*</sup>';
        template += '</label>';
        template += '<div class="form-control" ng-click="methods.wizard()">';
        template += '<div class="pull-right">';
        template += '<i class="material-icons md-18" ng-if="!view.display">arrow_drop_down</i>';
        template += '<i class="material-icons md-18" ng-if="view.display">arrow_drop_up</i>';
        template += '<a href="javascript:;" ng-if="view.remove && view.item" ng-click="methods.remove($event)">';
        template += '<i class="material-icons md-18">clear</i>';
        template += '</a>';
        template += '</div>';
        template += '<span ng-if="view.item != null">';
        template += '<span ng-repeat="field in fields"><span ng-if="!view.item[field]">{{field}}</span><span ng-if="view.item[field]">{{view.item[field]}}</span></span>';
        template += '</span>';
        template += '<span ng-if="view.item == null" class="placeholder">{{placeholder}}</span>';
        template += '<input type="hidden" ng-model="view.item.id" ng-required="{{req}}" />';
        template += '</div>';
        template += '</div>';
        return {
            template: template,
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                label: "@",
                options: "=",
                fields: "=",
                placeholder: "@",
                hasError: "=",
                req: "@",
                model: "=",
                orderBy: "@",
                remove: "="
            },
            link: function postLink(scope, element, attrs) {
                scope.view = {
                    keywords: '',
                    item: null,
                    display: false,
                    isValid: true,
                    remove: (angular.isDefined(scope.remove && scope.remove === true))
                };
                scope.methods = {
                    init: function () {
                        $timeout(function () {
                            scope.view.item = $filter('getBy')(scope.options, 'id', scope.model);
                        }, 100);
                    },
                    remove: function ($event) {
                        $event.stopPropagation();
                        scope.view.item = null;
                        scope.model = null;
                    },
                    wizard: function () {
                        var template = '';
                        template += '<div class="modal-header">';
                        template += '<button type="button" class="close" ng-click="methods.close()" aria-label="Close">';
                        template += '<span aria-hidden="true">';
                        template += '<i class="material-icons md-24">clear</i>';
                        template += '</span>';
                        template += '</button>';
                        template += '<h4 class="modal-title">{{view.placeholder}}</h4>';
                        template += '</div>';
                        template += '<div class="modal-body">';
                        template += '<div class="form-group">';
                        template += '<input type="text" placeholder="{{\'Search\'| translate}}" ng-model="keywords" class="form-control" />';
                        template += '</div>';
                        template += '<div class="form-options" ng-scrollbars>';
                        template += '<ul class="list-unstyled">';
                        template += '<li ng-repeat="option in view.options| filter: keywords | orderBy:(view.orderBy) ? orderBy : view.fields[0]" ng-click="methods.select(option)" ng-class="view.item.id == option.id ? \'active\' : \'\'">';
                        template += '<span ng-repeat="field in view.fields"><span ng-if="!option[field]">{{field}}</span><span ng-if="option[field]">{{option[field]}}</span></span>';
                        template += '</li>';
                        template += '</ul>';
                        template += '</div>';
                        template += '</div>';
                        template += '<div class="modal-footer">';
                        template += '<button type="button" class="btn btn-primary" ng-click="methods.valid()" ng-disabled="view.item.id == null" translate>Select</button>';
                        template += '</div>';

                        var modalInstance = $uibModal.open({
                            template: template,
                            controller: ['$scope', '$uibModalInstance', 'view', function ($scope, $uibModalInstance, view) {
                                    $scope.view = angular.copy(view);
                                    $scope.view.orderBy = scope.orderBy;
                                    $scope.methods = {
                                        init: function () {
                                            if ($scope.view.item != null) {
                                                $scope.view.selected = true;
                                            }
                                        },
                                        select: function (item) {
                                            $scope.view.item = item;
                                            $scope.view.selected = true;
                                        },
                                        valid: function () {
                                            $uibModalInstance.close($scope.view.item);
                                        },
                                        close: function () {
                                            $uibModalInstance.dismiss('cancel');
                                        }
                                    };
                                    $scope.$on('modal.closing', function (event, reason, closed) {
                                        if ($scope.view.selected) {
                                            $scope.view.item = null;
                                        }
                                        if (view.item == null) {
                                            scope.view.isValid = false;
                                        }
                                    });
                                }],
                            size: 'sm',
                            windowClass: 'modal-select',
                            backdrop: true,
                            resolve: {
                                view: function () {
                                    return {
                                        item: scope.view.item,
                                        fields: scope.fields,
                                        selected: false,
                                        options: scope.options,
                                        placeholder: scope.placeholder
                                    };
                                }
                            }
                        });
                        // Get the element returned by the modal
                        modalInstance.result.then(function (item) {
                            scope.view.display = false;
                            if (item != null) {
                                scope.view.isValid = true;
                                scope.model = item.id;
                                scope.view.item = item;
                            } else {
                                scope.view.isValid = false;
                            }
                        });
                    }
                };
                scope.methods.init();
            }
        };
    }
]);